var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var server = require('../../bin/www'); //aca nos traemos el modulo de server para que levante solo.
var request = require('request');

var base_url = "http://localhost:3000/api/bicicletas";

beforeEach(function(){console.log(`testeando…`); }); // asi aprobas -- beforeEach(function(){console.log(‘testeando…’); });

describe('Bicicleta API', () => {

    beforeAll(function(done){

        mongoose.connection.close().then(() => {
        var mongoDB = 'mongodb://localhost:testdb';
            mongoose.connect(mongoDB, {useNewUrlParser: true});

            const db = mongoose.connection;
            db.on('error', console.error.bind(console, 'connection error'));
            db.once('open', function(){
                console.log('We are connected to test database');
                done();
            });
        });
    });
    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if(err) console.log(err);
            done();
        });
    });
    

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function(error, response, body){
                var result = JSON.parse(body);
                console.log(response.statusCode);
                console.log(response.statusMessage);
                console.log(body);
                expect(response.statusCode).toBe(200);
                done();

            });

        });

    });

    describe('POST BICICLETAS /create', () => {  // ruta / create y el callback es --- ()
        it('STATUS 200', (done) => {               // hasta que done no se ejecuta no se termina el test. Ganamos, como todo es asincronico el request puede ser que no se ejecute bien y termine el test..
            var headers = {'content-type' : 'application/json'};
            var aBici = '{ "code": 10, "color": "rojo", "modelo" : "urbano", "lat": -34, "lng": -54 }';  
     
            request.post({
                      headers: headers,
                      url:  base_url + '/create',
                      body: aBici
                    }, function(error, response, body){
                        console.log(response.statusCode);
                        console.log(response.statusMessage);
                        console.log(body);
                        expect(response.statusCode).toBe(200);
                        var bici = JSON.parse(body);
                        console.log(bici);
                        expect(bici.bicicleta.color).toBe('rojo');
                        expect(bici.bicicleta.ubicacion[0]).toBe(-34);
                        expect(bici.bicicleta.ubicacion[1]).toBe(-54);
                        done();
            });

        });

    });

    describe('DELETE BICICLETAS /delete', () => {  // ruta / create y el callback es --- ()
        it('STATUS 204', (done) => {               // hasta que done no se ejecuta no se termina el test. Ganamos, como todo es asincronico el request puede ser que no se ejecute bien y termine el test..
            var headers = {'content-type' : 'application/json'};
            var aBici = `{ "code": 10, "color": "rojo", "modelo" : "urbano", "lat": -34, "lng": -54 }`;  
            var id = `{ "code": 10}`; 
     
            request.post({
                        headers: headers,
                        url: base_url + '/create',
                        body: aBici
                    }, function(error, response, body){
                        expect(response.statusCode).toBe(200);

                    })
            request.delete({
                            headers: headers,
                            url:  base_url + '/delete',
                            body: id
                        }, function(error, response, body){
                            expect(response.statusCode).toBe(204);
                   
                        done();
            });

        });

    });
});