var mongoose = require ('mongoose');
const uniqueValidator = require('mongoose-unique-validator');
var Reserva = require ("./reserva");
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const saltRounds = 10;


const Token = require('../models/Token');
const mailer = require('../mailer/mailer');

var Schema = mongoose.Schema;


const validateEmail = function(email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email);
}

var usuarioSchema = new Schema({
    nombre: {
       type: String,
       trim: true,
       required: [true, 'El nombre es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'El email es obligatorio'],
        lowercase: true,
        unique: true,   //para que no se repita los mail, tenemos que instalar la libreria
        validate: [validateEmail, 'Por favor ingresar un email valido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'El Password es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
    
});


usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario'}); // son modulos o libreria de mongoose

usuarioSchema.pre('save', function(next){
    if (this.isModified('password')){
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function(password){
    return bcrypt.compareSync (password, this.password);
}


usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb){
    var reserva = new Reserva({usuario: this.id, bicicleta: biciId, desde: desde, hasta: hasta})
    console.log(reserva);
    reserva.save(cb);
}
usuarioSchema.statics.add = function(aUser, cb){
    console.log(aUser);
    this.create(aUser,cb);
};

usuarioSchema.statics.updateUser = function(userObj, cb){
    console.log(userObj.nombre);
    this.updateOne({ _id: userObj._id }, {$set: {nombre: userObj.nombre}}, cb);
}



usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});  //creamos un astring en hexa
    const email_destination = this.email;
    Token.save(function (err) {   //ahora si vamos a persistri.
        if (err) { return console.log(err.message); }

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link: \n' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + '\n'
        };

        mailer.sendMail(mailOptions, function (err) {
            if (err) { return console.log(err.message);}

            console.log('Se ha enviado un mail de benvenida a:' + email_destination + '.');
        });
    });


}

usuarioSchema.methods.resetPassword = function (cb){
    const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if (err) { return cb(err);}

        const mailOptions = {
            from: 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Reseteo de password de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link: \n' + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '\n'
        };
        mailer.sendMail(mailOptions, function (err) {
            if (err) { return console.log(err.message);}

            console.log('Se ha enviado un mail de benvenida a:' + email_destination + '.');
        });
        cb(null);
    });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){  // Le ponemos condition => profile porque desde el punto de vista del modelo lo tendremos como condicion de busqueda o creacion
    const self = this;
    console.log(condition);

    self.findOne({  // esto ya es mongoose
        $or:[
            {'googleId': condition.id},
            {'email': condition.emails[0].value}
    ]}, (err, result) => {
// parte de Login:
        if (result) {
            callback(err, result)
// parte de registro:
        }else {  // en caso de exito
            console.log('--------- CONDITION ---------');
            console.log(condition);
            let values = {};
            values.googleId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');  // seria mejor crear un objeto propio q sea de cada provider
            console.log('--------- VALUE -------');
            console.log(values);
            self.create(values, (err, result) => {
                if (err) { console.log(err);}
                return callback(err, result)
            })
        }
    });
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback) {
    const self = this;
    console.log(condition);

    self.findOne({
        $or: [
            { 'facebookId': condition.id },
            { 'email': condition.emails[0].value }
        ]
    }, 
    (err, result) => {
        if (result) {
            callback(err, result);
        } else {
            let values = {};
            console.log('=============== CONDITION ===============');
            console.log(condition);

            values.facebookId = condition.id;
            values.email = condition.emails[0].value;
            values.nombre = condition.displayName || 'SIN NOMBRE';
            values.verificado = true;
            values.password = crypto.randomBytes(16).toString('hex');

            console.log('=============== VALUES ===============');
            console.log(values);

            self.create(values, function (err, user) {
                if (err) {
                    console.log(err);
                }

                return callback(err, user);
            });
        }
    });
};


module.exports = mongoose.model('Usuario', usuarioSchema);
