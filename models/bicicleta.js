var mongoose = require ('mongoose');
var Schema = mongoose.Schema;

var bicicletaSchema = new Schema({
    code: Number,
    color: String,
    modelo: String,
    ubicacion: {
        type: [Number], index: {type: '2dsphere', sparse: true}  // Definimos el esquema con el index
    }
});



//ahora vamos a ir armando cada una de las sfunciones. En sintaxis MONGO. Creamos metodos de instancias

bicicletaSchema.statics.createInstance = function(code, color, modelo, ubicacion){  //crear instancia. Estatico para crear bicicletas. Devuelve el nuevo obejto
    return new this({
        code: code,
        color: color,
        modelo: modelo,
        ubicacion: ubicacion
    });
};



bicicletaSchema.method.toString = function(){
        return 'code: ' + this.code + ' | color: ' + this.color;
};


bicicletaSchema.statics.allBicis = function(cb){ // cb es el CallBack
    return this.find({}, cb);
};


bicicletaSchema.statics.add = function(aBici, cb){
    this.create(aBici , cb);
};

bicicletaSchema.statics.findByCode = function(aCode, cb){  // buscamos en la base de mongo. LE pasamos el codigo. Ver funciones de mongo
    return this.findOne({code: aCode} , cb);    //criterio de filtrado es el aCode
};

bicicletaSchema.statics.removeByCode = function(aCode, cb){
    return this.deleteOne({code: aCode} , cb);
};

module.exports = mongoose.model(`Bicicleta`, bicicletaSchema);  // exportamos el modelo de "bicicleta"

