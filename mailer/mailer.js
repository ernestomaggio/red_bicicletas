// Use at least Nodemailer v4.1.0
const nodemailer = require('nodemailer');
const sgTransport = require('nodemailer-sendgrid-transport');

let mailConfig;
if (process.env.NODE_ENV === 'production'){
    const options = {
        auth: {
            api_key: process.env.SENDGRID_API_KEY
        }
    }
    mailConfig = sgTransport(options);
} else{
    if (process.env.NODE_ENV == 'staging') {
        console.log('XXXXXXXXXXXX');
        const options = {
            auth: {
                api_key: process.env.SENDGRI_API_KEY
            }
        }
    mailConfig = sgTransport(options);

    }else {
        // all emails are catched by ethera.email
        mailConfig = {
            host: 'smtp.ethereal.email',
            port: 587,
            auth: {
                user: process.env.ethereal_user,
                pass: process.env.ethereal_psw
            }
        };
    }
}


module.exports = nodemailer.createTransport(mailConfig);

