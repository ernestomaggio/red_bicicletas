# Bienvenido a Express
# Descripción del proyecto

Se trabaja del lado servidor, en el backend, desarrollando el soporte que toda aplicación necesita para lidiar con la persistencia de la información, 
el setup de un servidor web, la creación de una API REST, autenticación y autorización, y la integración de librerías de terceros. 
Utiliza Express para el servidor web, y una base de datos NoSQL orientada a documentos: MongoDB.
ODM con Mongoose y las típicas tareas CRUD sobre Mongo.

# Instalación
Ingresa a la terminal:

npm install

Se instalarán todas las dependencias necesarias para el proyecto.
## Setup

Just run the setup script to configure the app:

```bash
  npm install / yarn install
```

To run the app:

```bash
  npm devstart / yarn devstart
```

## Endpoints API

```bash
  http://localhost:3000/api/bicicletas
```

```bash
    http://localhost:3000/api/bicicletas/create
```

```bash
  http://localhost:3000/api/bicicletas/delete
```

```bash
  http://localhost:3000/api/bicicletas/:id/update
```
