var express = require('express');
var router = express.Router();
var bicicletaController = require('../controllers/bicicleta');

// El verbo http que vamos a utlizar

router.get('/', bicicletaController.bicicleta_list); // Luego en App idicarle que lo use
router.get('/create',bicicletaController.bicicletas_create_get);
router.post('/create',bicicletaController.bicicletas_create_post);
router.get('/:id/update',bicicletaController.bicicletas_update_get);
router.post('/:id/update',bicicletaController.bicicletas_update_post);
router.delete('/delete',bicicletaController.bicicletas_delete_post); // el :id le indicamos que es un parametro

module.exports = router;



